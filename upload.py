import os
from datetime import datetime
from flask import Flask, request, render_template, jsonify
from updated_code.tesseract_ocr import image_to_text
from updated_code.general_data_form_extraction import generalized_form_data_extraction
from werkzeug.utils import secure_filename
from PIL import Image
import pdf2image
import shutil
from autocorrect import spell

import csv
app = Flask(__name__)
APP_ROOT = "updated_code/output/" # project abs path
dir_path = os.path.dirname(os.path.realpath(__file__))
app.config['UPLOAD_FOLDER'] = os.path.join(dir_path, 'output')

print(dir_path)

@app.route("/")
def index():
    return render_template("index.html")


@app.route("/upload_page", methods=["GET"])
def upload_page():
    return render_template("upload.html")

@app.route("/process", methods=["GET"])
def process_page():
    return render_template("complete.html")

@app.route("/ajax/upload", methods=["POST"])
def upload():
    print(os.path.realpath(__file__))
    target = os.path.join(dir_path, 'output')
    if not os.path.isdir(target):
        os.mkdir(target)
    print(request.files.getlist("files[]"))

    files = request.files.getlist('files[]')
    for file in files:
        if file:
            filename = secure_filename(file.filename)
            file.save(os.path.join(app.config['UPLOAD_FOLDER'], filename))

    result=0;
    open("/home/nitin/Downloads/synechron/updated_code/updated_code/output.csv", 'w').close()

    print("before change of pwd",os.getcwd())
    path=os.getcwd()
    os.chdir("/home/nitin/Downloads/synechron/updated_code/updated_code/output")
    print("after change of pwd",os.getcwd())
    result=0;
    filenames=[]
    accuracy=[]
    for filename in os.listdir(os.getcwd()):
        print("{} is the file name".format(filename))
        ext=os.path.splitext(filename)[1]
        print("{} is file extension".format(ext))

        if(ext=='.pdf'):
            name=os.path.splitext(filename)[0]
            ext='.jpg'
            print(name)
            pil_file=pdf2image.convert_from_path(filename)
            shutil.move(filename,"/home/nitin/Downloads/synechron/updated_code/updated_code/converted")
            for image in pil_file:
                image.save(os.path.splitext(filename)[0]+ext)
            filename=name+ext
        with open(filename,'r') as f:
            text=image_to_text(filename,psm=3)
            #print(text)
            text = ' '.join(text.replace('-', '').replace('|', '').replace('.', '').replace('_', '').replace("'", '').replace(",",'').replace('"','').split())
            print(text)
            info_dict = generalized_form_data_extraction(text)

            #print(info_dict)
            try:
               with open("/home/nitin/Downloads/synechron/updated_code/updated_code/output.csv", 'a', newline='') as f:
                 print("file opened")
                 csv_writer = csv.DictWriter(f, fieldnames=info_dict.keys())
                 csv_writer.writeheader()
                 csv_writer.writerow(info_dict)
                 result=result+1
                 filenames.append(filename)
                 accuracy.append(50)
            except IOError:
                 print("I/O error")
    os.chdir(path)
    print("before rendering")
    return jsonify({
         "status": "Success",
         "filenames":filenames,
        "accuracy":accuracy
     })
    #return render_template('complete.html',result=str(result))


if __name__ == "__main__":
    app.run(port=4555, debug=True)

