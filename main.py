from updated_code.general_data_form_extraction import generalized_form_data_extraction
from updated_code.tesseract_ocr import image_to_text


if __name__== "__main__":
    imPaths=["input/"]

    for img in imPaths:
        text =image_to_text(img,psm=3)

        text = ' '.join(text.replace('-', '').replace('|','').replace('.','').replace('_','').replace("'",'').split())
        print(text)
        print('-'*80)

        print(generalized_form_data_extraction(text))
        print('='*80)