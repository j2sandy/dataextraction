import cv2
import pytesseract
import numpy as np


def img_preprocess(imPath, thresh=False, blur=True):
    # load the example image and convert it to grayscale
    image = cv2.imread(imPath)
    #image=cv2.resize(image,None,fx=2.5,fy=2.5,interpolation=cv2.INTER_CUBIC)
    #image=increase_contrast(image)
    gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)


    # check to see if we should apply thresholding to preprocess the image
    if thresh:
        gray = cv2.threshold(gray, 0, 255, cv2.THRESH_BINARY_INV | cv2.THRESH_OTSU)[1]
        #gray = remove_lines(gray)


    # make a check to see if median blurring should be done to remove noise
    elif blur:
        #gray = cv2.bilateralFilters(gray,9,75,75)
        gray = cv2.GaussianBlur(gray,(5,5),0)

    return gray

def increase_contrast(img):
    imghsv = cv2.cvtColor(img, cv2.COLOR_BGR2HSV)
    imghsv[:, :, 2] = [[max(pixel - 19, 0) if pixel < 190 else min(pixel + 25, 255) for pixel in row] for row in
                       imghsv[:, :, 2]]


    img = cv2.cvtColor(imghsv, cv2.COLOR_HSV2BGR)
    return img

def remove_lines(img):
    # Remove horizontal lines
    horizontal_kernel = cv2.getStructuringElement(cv2.MORPH_RECT, (40, 1))
    remove_horizontal = cv2.morphologyEx(img, cv2.MORPH_OPEN, horizontal_kernel, iterations=3)
    cnts = cv2.findContours(remove_horizontal, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)
    cnts = cnts[0] if len(cnts) == 2 else cnts[1]
    for c in cnts:
        cv2.drawContours(img, [c], -1, (255, 255, 255), 5)

    # Remove vertical lines
    vertical_kernel = cv2.getStructuringElement(cv2.MORPH_RECT, (1, 40))
    remove_vertical = cv2.morphologyEx(img, cv2.MORPH_OPEN, vertical_kernel, iterations=2)
    cnts = cv2.findContours(remove_vertical, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)
    cnts = cnts[0] if len(cnts) == 2 else cnts[1]
    for c in cnts:
        cv2.drawContours(img, [c], -1, (255, 255, 255), 7)
    return img

def image_to_text(imPath, lang='eng', oem=1, psm=3):
    # Uncomment the line below to provide path to tesseract manually
   # pytesseract.pytesseract.tesseract_cmd = '/usr/local/Cellar/tesseract/4.1.0/bin/tesseract'

    # Define config parameters.
    # '-l eng'  for using the English language
    # '--oem 1' for using LSTM OCR Engine
    # https://github.com/tesseract-ocr/tesseract/wiki/ImproveQuality#page-segmentation-method
    # config = ('-l eng --oem 1 --psm 3')
    config = '-l {0} --oem {1} --psm {2}'.format(lang, oem, psm)

    # preprocess the input image
    img = img_preprocess(imPath)

    # Read image from disk (uncomment the line in case of reading image from path)
    # img = cv2.imread(imPath, cv2.IMREAD_COLOR)

    # Run tesseract OCR on image
    text = pytesseract.image_to_string(img, config=config)

    # return recognized text
    return text
