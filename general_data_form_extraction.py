def find_branch_account_fa(text):
    start=text.index("Morgan Stanley")
    end=text.find("TOD")
    start=start+14
    substring=text[start:end]
    substring=substring.strip()
    strings=substring.split(" ")
    return strings[0],strings[1],strings[2]


def find_roi(para, keywords):
    list_of_indexes = []
    for term in keywords:
        list_of_indexes.append(para.index(term))

    roi_start = min(list_of_indexes)
    #print(roi_start)
    roi_end = para.index("Ifa sole Account")
    roi = para[roi_start:roi_end]
    return roi


## well now what we can do we can find a par
def extract_info(complete_roi, keywords):
    keys_dict = {}
    checked_keys = []

    text = complete_roi

    for item1 in keywords:
        for item2 in keywords:
            combined_key1 = item1 + " " +item2
            if combined_key1 in text:

                keys_dict[combined_key1] = text.index(combined_key1)
                checked_keys.append(item1)
                checked_keys.append(item2)
    return keys_dict, checked_keys


# This is the data extraction part
# the last element which didn't have any pair is extracted first
# Then if any pair has name or beneficiary, then name will be extracted
# assuming that the name contain only 2 words
def parse_complete_info(roi2, combined_keys_loc2, last_item):
    
    form_data_dict = {}

    last = list(last_item)[0]

    last_index = roi2.index(last) + len(last)
    form_data_dict[last] = roi2[last_index:]
    keys = []
    values = []
    for i in combined_keys_loc2:
        keys.append(i)
        values.append(combined_keys_loc2[i])
    # print(keys)
    # print(values)
    number_of_turns = len(keys) - 1
    sent = []
    #
    for i in range(number_of_turns):
        indexst = values[i]+len(keys[i])
        indexend = values[i+1]
        sent.append(roi2[indexst:indexend].strip())
    indexst = values[number_of_turns] + len(keys[number_of_turns])
    indexend = roi2.index(last)
    sent.append(roi2[indexst:indexend].strip())
    # print(sent)
    for i in range(len(sent)):
        tag = keys[i]
        if "Owner Full Name" in tag:
            # print("yes")
            st_tag_index = tag.index("Owner Full Name")

            strings = sent[i].split(" ")
            # print(strings)
            if st_tag_index==0:
                form_data_dict["Owner Full Name"] = strings[0]+' '+strings[1]
                form_data_dict[tag[st_tag_index+len("Owner Full Name"):]] = " ".join(strings[2:])
            else:
                form_data_dict["Owner Full Name"] =" ".join( strings[-2:])
                form_data_dict[tag[0:st_tag_index ]] = " ".join(strings[0:-2])

        if "Beneficiarys Full Name" in tag:
            # print("yes",i)
            st_tag_index = tag.index("Beneficiarys Full Name")
            strings = sent[i].split(" ")
            # print(strings)
            if st_tag_index==0:
                form_data_dict["Beneficiarys Full Name"] = strings[0]+' '+strings[1]
                form_data_dict[tag[st_tag_index+len("Beneficiarys Full Name"):]] = " ".join(strings[2:])
            else:
                form_data_dict["Beneficiarys Full Name"] =" ".join( strings[-2:])
                form_data_dict[tag[0:st_tag_index ]] = " ".join(strings[0:-2])
    return form_data_dict


def generalized_form_data_extraction(whole_text):
    keywords = ['Identification', 'Owner Full Name', 'Address', 'Beneficiarys Full Name', 'Social Security Number']
    roi = find_roi(whole_text, keywords)
    combined_keys_loc, checked_combined_keys = extract_info(roi, keywords)
    #print(combined_keys_loc)
    last_item = set(keywords) - set(checked_combined_keys)

    #print(checked_combined_keys)
    form_dict = parse_complete_info(roi, combined_keys_loc, last_item)
    branch,account,fa=find_branch_account_fa(whole_text)
    form_dict['Branch No']=branch
    form_dict['Account No']=account
    form_dict['FA No']=fa
    new_dict={}
    for k,v in form_dict.items():
        print(k,v)
        new_k=k.strip()
        new_v=v.strip()
        new_dict[new_k]=new_v

    form_dict.clear();
    form_dict['Branch No'] = new_dict['Branch No']
    form_dict['Account No'] = new_dict['Account No']
    form_dict['FA No'] = new_dict['FA No']
    form_dict['Identification']=new_dict['Identification']
    form_dict['Owner Full Name'] = new_dict['Owner Full Name']
    form_dict['Address'] = new_dict['Address']
    form_dict['Beneficiarys Full Name'] = new_dict['Beneficiarys Full Name']
    form_dict['Social Security Number'] = new_dict['Social Security Number']

    return form_dict
